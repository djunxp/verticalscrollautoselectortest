package com.test.verticalscrollautoselectortest;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.verticalscrollautoselectortest.VerticalScrollAutoSelector.AutoSelectorAdapter;

public class MainActivity extends Activity {

	private class TestAutoSelectorAdapter extends AutoSelectorAdapter {

		private Context context;
		private int count = 0;

		private static final int ITEMS_COUNT_PER_GROUP = 5;

		public TestAutoSelectorAdapter(Context context, int count) {
			this.context = context;
			this.count = count;
		}

		@Override
		public int getItemsCountPerGroup() {
			return ITEMS_COUNT_PER_GROUP;
		}

		@Override
		public int getCount() {
			return count;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public int getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = new TextView(context);
			}

			if (convertView instanceof TextView) {
				TextView t = (TextView) convertView;
				t.setText(String.valueOf(position));
				t.setTextColor(Color.BLACK);
				t.setGravity(Gravity.CENTER);
			}

			System.out.println("position " + String.valueOf(position) + " ok.");// debug

			return convertView;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		VerticalScrollAutoSelector selector = (VerticalScrollAutoSelector) findViewById(R.id.testVSAS);
		selector.setAdapter(new TestAutoSelectorAdapter(this, 100));
		System.out.println("adapter created.");// debug
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
